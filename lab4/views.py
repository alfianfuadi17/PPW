from django.shortcuts import render

my_name = 'Alfian Fuadi Rafli'

def home(request):
    response = {'name' : my_name}
    return render(request, 'home.html', response)

def register(request):
    response = {'name' : my_name}
    return render(request, 'register.html', response)
