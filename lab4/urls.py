from django.urls import path

from . import views

app_name = 'lab4'
urlpatterns = [
    path('', views.home, name = 'home'),
    path('register/', views.register, name = 'register'),
]
